# Compilation pour l'agrégation d'informatique

Cours : [Christophe Alias](http://perso.ens-lyon.fr/christophe.alias/), TD : [Nicolas Chappe](http://perso.ens-lyon.fr/nicolas.chappe/). 

# Planning

* [Introduction](cours/cours-0-intro.pdf) 
* [Cours 1 : Analyse lexicale](cours/cours-1-lexer.pdf) ([handout](cours/cours-1-lexer-handout.pdf)) ([TD](td/td1-lexer.pdf))
* [Cours 2 : Analyse syntaxique](cours/cours-2-parser.pdf) ([handout](cours/cours-2-parser-handout.pdf)) ([TD](td/td2-parser.pdf))
* [Cours 3 : Analyse sémantique](cours/cours-3-sem.pdf) ([handout](cours/cours-3-sem-handout.pdf)) ([TD](td/td3-sem.pdf))
* Cours 4 : Génération de code
  * [Traduction dirigée par la syntaxe](cours/cours-4-translation.pdf)
  * [Activations](cours/cours-5-activations.pdf) ([TD traduction/activations](td/td4-codegen.pdf))
  * [Données structurées](cours/cours-6-data-layout.pdf) ([TD](td/td5-data-layout.pdf))
* [Cours 5 : Thèmes](cours/cours-7-topics.pdf) ([handout](cours/cours-7-topics-handout.pdf))

# Ressources
* [Interactive playground](https://edu-insa-4a-compilation-vm.netlify.app/) pour la machine à pile
*  Un [compilateur simple](src/simple-compiler.tgz) avec la machine à pile, en C++.
*  Un [compilateur plus sérieux](https://perso.ens-lyon.fr/christophe.alias/archi/digcc.tgz) et son [architecture cible](https://perso.ens-lyon.fr/christophe.alias/archi_lyon1.html). Ce compilateur utilise une machine à registres comme machine abstraite intermédiaire.
* Une chouette [chaîne youtube](https://www.youtube.com/channel/UCLRKqOD--AKLr3rzjXXeQ_Q) pour aller plus loin!

# Sujets blancs
* [sujet de type 3 (extrait), 2022](sujets/sujet.pdf)